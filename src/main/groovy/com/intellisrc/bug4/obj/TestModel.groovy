package com.intellisrc.bug4.obj

import com.intellisrc.bug4.lib.Model
import groovy.transform.CompileStatic

/**
 * @since 2023/05/26.
 */
@CompileStatic
class TestModel extends Model {
    int id = 0
}
