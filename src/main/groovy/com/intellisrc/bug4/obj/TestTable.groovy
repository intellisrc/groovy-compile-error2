package com.intellisrc.bug4.obj

import com.intellisrc.bug4.lib.Table
import groovy.transform.CompileStatic

/**
 * @since 2023/05/26.
 */
@CompileStatic
class TestTable extends Table<TestModel> {
}
