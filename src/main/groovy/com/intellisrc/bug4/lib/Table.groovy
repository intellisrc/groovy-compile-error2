package com.intellisrc.bug4.lib

import groovy.transform.CompileStatic

/**
 * @since 2023/05/26.
 */
@CompileStatic
class Table<T extends Model> {
    static interface ChunkReader<T> {
        void call(List<T> row)
    }
    void getAll(ChunkReader<T> reader) {
        List<T> chunk = [] as List<T>
        reader.call(chunk)
    }
}
