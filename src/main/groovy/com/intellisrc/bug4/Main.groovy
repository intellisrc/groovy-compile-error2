package com.intellisrc.bug4

import com.intellisrc.bug4.obj.TestModel
import com.intellisrc.bug4.obj.TestTable
import groovy.transform.CompileStatic

/**
 * @since 2023/05/26.
 */
@CompileStatic
class Main {
    static void main(String[] args) {
        TestTable table = new TestTable()
        table.getAll({
            List<TestModel> list ->
                list.each {
                    TestModel tm ->
                        println(tm.id)
                }
        })
    }
}
